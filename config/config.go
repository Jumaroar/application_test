package config

import (
	"encoding/json"
	"os"
)

type Configuration struct {
	Secret             string `json:"secret"`
	BitSize            int    `json:"bit_size"`
	DatabaseConnection string `json:"database_connection"`
	DatabaseDriver     string `json:"database_driver"`
	Port               string `json:"port"`
}

func Config() (Configuration, error) {
	var config Configuration
	configFile, err := os.Open("conf.json")
	defer configFile.Close()
	if err != nil {
		return config, err
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)
	return config, nil
}

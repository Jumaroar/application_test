package v1

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"net/http"

	"../../config"
	"../../helpers"
	"../../models"
)

func Index(w http.ResponseWriter, r *http.Request) {
	queryString := r.URL.Query().Get("query")

	var keys []models.KeyResponse

	keys = models.GetKeys(keys, queryString, w)

	helpers.RespondwithJSON(w, http.StatusOK, keys)
}

func Create(w http.ResponseWriter, r *http.Request) {

	configuration, e := config.Config()
	if e != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, e.Error())
	}

	var key models.Key
	var response models.TextResponse

	err := json.NewDecoder(r.Body).Decode(&key)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	reader := rand.Reader
	bitSize := configuration.BitSize

	rsaKey, err := rsa.GenerateKey(reader, bitSize)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	key.PublicKey = base64.StdEncoding.EncodeToString(helpers.EncodePublicPem(rsaKey.PublicKey, w))
	key.PrivateKey = base64.StdEncoding.EncodeToString(helpers.CipherAES256(helpers.EncodePrivatePem(rsaKey), configuration.Secret, w))

	response.Content = models.InsertKey(key.Name, key.PublicKey, key.PrivateKey, w)

	helpers.RespondwithJSON(w, http.StatusCreated, response)
}

func Cypher(w http.ResponseWriter, r *http.Request) {

	var request models.TextRequest
	var response models.TextResponse

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	key, err := models.GetKey(request.Id, w)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	publicKey := helpers.ConvertPublicKey(key.PublicKey, w)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	requestText, err := base64.StdEncoding.DecodeString(request.Text)

	encryptedText := helpers.CipherRSA(requestText, publicKey, w)

	response.Content = base64.StdEncoding.EncodeToString(encryptedText)

	helpers.RespondwithJSON(w, http.StatusOK, response)
}

func Decypher(w http.ResponseWriter, r *http.Request) {

	var request models.TextRequest
	var response models.TextResponse

	err := json.NewDecoder(r.Body).Decode(&request)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	key, err := models.GetKey(request.Id, w)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	configuration, err := config.Config()
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}
	privateKey := helpers.ConvertPrivateKey(key.PrivateKey, w, configuration.Secret)
	base64DecodedText, err := base64.StdEncoding.DecodeString(request.Text)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	decryptedText := helpers.DecipherRSA([]byte(base64DecodedText), privateKey, w)

	response.Content = base64.StdEncoding.EncodeToString(decryptedText)

	helpers.RespondwithJSON(w, http.StatusOK, response)
}

package db

import (
	"database/sql"
	"log"

	"../config"
	_ "github.com/lib/pq"
)

var Db *sql.DB

func init() {
	Db = Connect()
}

func Connect() (db *sql.DB) {
	configuration, err := config.Config()
	if err != nil {
		log.Panicf("logging err: %s\n", err.Error())
	}
	db, error := sql.Open(configuration.DatabaseDriver, configuration.DatabaseConnection)
	if error != nil {
		log.Fatal("There was an error connecting to the database.", error)
	}
	return db
}

func CreateTable(db *sql.DB) {
	db.Exec("CREATE DATABASE application_test;")
	db.Exec("CREATE SEQUENCE IF NOT EXISTS key_seq;")
	db.Exec("CREATE TABLE IF NOT EXISTS keys(id INT PRIMARY KEY DEFAULT nextval('key_seq'),name STRING(255) UNIQUE NOT NULL,public_key TEXT NOT NULL,private_key TEXT NOT NULL)")
}

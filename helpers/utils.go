package helpers

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"crypto/x509"
	"encoding/asn1"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"io"
	"net/http"
)

func RespondwithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(code)
	w.Write(response)
}

func RespondWithError(w http.ResponseWriter, code int, msg string) {
	RespondwithJSON(w, code, map[string]string{"message": msg})
	panic(nil)
}

func EncodePublicPem(pubkey rsa.PublicKey, w http.ResponseWriter) []byte {
	asn1Bytes, err := asn1.Marshal(pubkey)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	var publicKey = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: asn1Bytes,
	}
	pemPublic := pem.EncodeToMemory(publicKey)
	return pemPublic
}

func EncodePrivatePem(key *rsa.PrivateKey) []byte {
	var privateKey = &pem.Block{
		Type:  "PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(key),
	}
	pemPrivateKey := pem.EncodeToMemory(privateKey)

	return pemPrivateKey
}

func CipherAES256(unciphered []byte, secret string, w http.ResponseWriter) []byte {
	key := []byte(secret)

	block, err := aes.NewCipher(key)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	nonce := make([]byte, gcm.NonceSize())

	_, err = io.ReadFull(rand.Reader, nonce)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	ciphered := gcm.Seal(nonce, nonce, unciphered, nil)
	return ciphered
}

func DecipherAES56(ciphered []byte, secret string, w http.ResponseWriter) []byte {
	key := []byte(secret)
	cipheredText := []byte(ciphered)

	block, err := aes.NewCipher(key)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	nonceSize := gcm.NonceSize()

	nonce, cipheredText := cipheredText[:nonceSize], cipheredText[nonceSize:]
	unciphered, err := gcm.Open(nil, nonce, cipheredText, nil)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}
	return unciphered
}

func ConvertPublicKey(PublicKey string, w http.ResponseWriter) *rsa.PublicKey {
	bytePublicKey, err := base64.StdEncoding.DecodeString(PublicKey)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	block, _ := pem.Decode(bytePublicKey)

	convertedPublicKey, err := x509.ParsePKCS1PublicKey(block.Bytes)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	return convertedPublicKey
}

func ConvertPrivateKey(PrivateKey string, w http.ResponseWriter, secret string) *rsa.PrivateKey {
	bytePrivateKey, err := base64.StdEncoding.DecodeString(PrivateKey)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}
	decipheredPrivateKey := DecipherAES56(bytePrivateKey, secret, w)
	block, _ := pem.Decode(decipheredPrivateKey)
	convertedPrivateKey, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}

	return convertedPrivateKey
}

func CipherRSA(message []byte, publicKey *rsa.PublicKey, w http.ResponseWriter) []byte {
	hash := sha512.New()
	cipheredText, err := rsa.EncryptOAEP(hash, rand.Reader, publicKey, message, nil)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}
	return cipheredText
}

func DecipherRSA(message []byte, privateKey *rsa.PrivateKey, w http.ResponseWriter) []byte {
	hash := sha512.New()
	plaintext, err := rsa.DecryptOAEP(hash, rand.Reader, privateKey, message, nil)
	if err != nil {
		RespondWithError(w, http.StatusBadRequest, err.Error())
	}
	return plaintext
}

package main

import (
	"log"
	"net/http"

	"./config"
	"./db"
	"./routes"
	"github.com/go-chi/chi"
)

func main() {
	configuration, err := config.Config()
	if err != nil {
		log.Panicf("logging err: %s\n", err.Error())
	}
	router := routes.Router()
	walkFunc := func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
		log.Printf("%s %s\n", method, route)
		return nil
	}

	if err := chi.Walk(router, walkFunc); err != nil {
		log.Panicf("logging err: %s\n", err.Error())
	}

	db.CreateTable(db.Db)

	log.Fatal(http.ListenAndServe(configuration.Port, router))

}

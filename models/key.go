package models

import (
	"database/sql"
	"fmt"
	"net/http"

	"../db"
	"../helpers"
)

type Key struct {
	Id         string `sql:",pk,unique,notnull"`
	Name       string `json:"name"`
	PublicKey  string `json:"public_key"`
	PrivateKey string `json:"private_key"`
}

func GetKey(Id int64, w http.ResponseWriter) (Key, error) {
	var key Key

	database := db.Db

	sqlStatement := "SELECT * FROM keys WHERE id = $1"
	row := database.QueryRow(sqlStatement, Id)
	er := row.Scan(&key.Id, &key.Name, &key.PublicKey, &key.PrivateKey)

	switch er {
	case sql.ErrNoRows:
		return key, er
	case nil:
		return key, nil
	default:
		return key, er
	}
}

func GetKeys(keys []KeyResponse, queryString string, w http.ResponseWriter) []KeyResponse {
	database := db.Db

	whereCondition := ""

	if queryString != "" {
		whereCondition = "WHERE lower(name) LIKE lower('%' || $1 || '%')"
	} else {
		queryString = "0"
		whereCondition = "OFFSET $1"
	}

	sqlStatement := fmt.Sprintf("%s %s", "SELECT id, name FROM keys", whereCondition)

	query, err := database.Query(sqlStatement, queryString)
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}
	defer query.Close()

	for query.Next() {
		var key KeyResponse
		err := query.Scan(&key.Id, &key.Name)
		if err != nil {
			helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
		}
		keys = append(keys, key)
	}

	return keys
}

func InsertKey(name string, publicKey string, privateKey string, w http.ResponseWriter) string {
	database := db.Db
	query, err := database.Prepare("INSERT INTO keys(name, public_key, private_key) VALUES ($1, $2, $3)")
	if err != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, err.Error())
	}
	_, er := query.Exec(name, publicKey, privateKey)
	if er != nil {
		helpers.RespondWithError(w, http.StatusBadRequest, er.Error())
	}
	defer query.Close()

	return "Key created successfully."
}

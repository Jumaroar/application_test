package models

type TextRequest struct {
	Id   int64  `json:"id"`
	Text string `json:"text"`
}

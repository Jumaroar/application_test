package rsa

import (
	rsa "../../controllers/v1"
	"github.com/go-chi/chi"
)

func Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Post("/create", rsa.Create)
	router.Post("/decypher", rsa.Decypher)
	router.Post("/cypher", rsa.Cypher)
	router.Get("/", rsa.Index)
	return router
}
